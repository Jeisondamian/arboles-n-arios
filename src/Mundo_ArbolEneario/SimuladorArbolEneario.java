/**
 * ---------------------------------------------------------------------
 * $Id: SimuladorArbolEneario.java,v 1.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingenieria de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Mundo_ArbolEneario;

import Colecciones_SEED.ArbolBinario;
import Colecciones_SEED.ArbolEneario;
import Graficos.ArbolEnearioG;
import java.util.Iterator;

/**
 * Clase que conecta la capa de presentación del Simulador con las Estructuras de Datos.
 * @author Uriel Garcia - Yulieth Pabon
 * @version 1.0
 */

public class SimuladorArbolEneario {
    
    private ArbolEneario<Integer> miArbolEne;
    
    public SimuladorArbolEneario(){
        this.miArbolEne = new ArbolEneario<Integer>();
    }
    
    public ArbolEnearioG crearArbolEnearioGrafico(ArbolEnearioG n) {
        n.crearArbol(this.miArbolEne.getRaiz());
        return (n);
     }

    public int conocerAltura() {
        return this.miArbolEne.getAltura();
    }    

    public int insertarDato(int dato, int padre, int tipo) {
        if(this.miArbolEne.esta(dato))
            return -1; //Dato repetido
        if(!this.estaVacioArbol() && !this.miArbolEne.esta(padre))
            return -4;
        boolean ins = tipo==0?this.miArbolEne.insertarHijo(padre, dato):this.miArbolEne.insertarHermano(padre, dato);
        if(ins){
            if(this.miArbolEne.getAltura()>6){
                this.miArbolEne.eliminar(dato);
                return -2; //Supera la altura 5
            }
            return 0; //Insercion correcta
        }
        return -3; //No se pudo insertar
    }

    public boolean estaVacioArbol() {
        return (this.miArbolEne.esVacio());
    }

    public boolean eliminarDato(int dato) {
        return (this.miArbolEne.eliminar(dato));
    }

    public boolean estaDatoenArbol(int dato) {
        return (this.miArbolEne.esta(dato));
    }

    public int conocerPeso() {
        return (this.miArbolEne.getPeso());
    }

    public int contarHojas() {
        return (this.miArbolEne.contarHojas());
    }

    public String obtenerHojas() {
        Iterator<Integer> it = this.miArbolEne.getHojas();
        String cad = "";
        while(it.hasNext()){
            cad+= it.next().toString();
            if(it.hasNext())
                cad+=", ";
            else
                cad+=".";
        }        
        return (cad);
    }

    public void podarHojas() {
        Iterator<Integer> it = this.miArbolEne.getHojas();
        while(it.hasNext()){
            Object obj = it.next();
            this.miArbolEne.eliminar(Integer.parseInt(String.valueOf(obj)));
        }        
    }

    public String recorridoPreorden() {
        Iterator<Integer> it = this.miArbolEne.preOrden();
        String cad = "";
        int i=0;
        while(it.hasNext()){
            cad+= it.next().toString();
            if(it.hasNext())
                cad+=", ";
            else
                cad+=".";
            if(i==15){
                cad+="\n";
            }
            i++;
        }        
        return (cad);
    }
    
    public String recorridoInorden() {
        Iterator<Integer> it = this.miArbolEne.inOrden();
        String cad = "";
        int i=0;
        while(it.hasNext()){
            cad+= it.next().toString();
            if(it.hasNext())
                cad+=", ";
            else
                cad+=".";
            if(i==15){
                cad+="\n";
            }
            i++;
        }        
        return (cad);
    }
    
    public String recorridoPostorden() {
        Iterator<Integer> it = this.miArbolEne.postOrden();
        String cad = "";
        int i=0;
        while(it.hasNext()){
            cad+= it.next().toString();
            if(it.hasNext())
                cad+=", ";
            else
                cad+=".";
            if(i==15){
                cad+="\n";
            }
            i++;
        }        
        return (cad);
    }
    
    public String recorridoPorNiveles() {
        Iterator<Integer> it = this.miArbolEne.impNiveles();
        String cad = "";
        int i=0;
        while(it.hasNext()){
            cad+= it.next().toString();
            if(it.hasNext())
                cad+=", ";
            else
                cad+=".";
            if(i==15){
                cad+="\n";
            }
            i++;
        }        
        return (cad);
    }

    public int conocerGordura() {
        return (this.miArbolEne.gordura());
    }
    
}
