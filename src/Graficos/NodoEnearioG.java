/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Graficos;

import javafx.scene.control.Label;
import javafx.scene.shape.Circle;

/**
 *
 * @author usuario
 */
public class NodoEnearioG {
    
    private Circle c;
    private Label l;
    private int info;
    private NodoEnearioG hijo;
    private NodoEnearioG hermano;

    /**
     *
     * @param info
     * @param c
     * @param l
     * @param izq
     * @param der
     */
    public NodoEnearioG(int info, Circle c, Label l, NodoEnearioG hijo, NodoEnearioG hermano) {
        this.c = c;
        this.l = l;
        this.info = info;
        this.hijo = hijo;
        this.hermano = hermano;
    }

    /**
     *
     * @return
     */
    public Circle getC() {
        return c;
    }

    /**
     *
     * @param c
     */
    public void setC(Circle c) {
        this.c = c;
    }

    /**
     *
     * @return
     */
    public Label getL() {
        return l;
    }

    /**
     *
     * @param l
     */
    public void setL(Label l) {
        this.l = l;
    }

    /**
     *
     * @return
     */
    public int getInfo() {
        return info;
    }

    /**
     *
     * @param info
     */
    public void setInfo(int info) {
        this.info = info;
        this.l.setText(info+"");
    }

    public NodoEnearioG getHijo() {
        return hijo;
    }

    public void setHijo(NodoEnearioG hijo) {
        this.hijo = hijo;
    }

    public NodoEnearioG getHermano() {
        return hermano;
    }

    public void setHermano(NodoEnearioG hermano) {
        this.hermano = hermano;
    }

   
    
    
}
